//Delete comment and add

function toggleModal() {
	let comments = document.querySelectorAll('.comment__section');

	let modalBox = document.querySelectorAll('.modal__box');
	let modalBtn = document.querySelectorAll('.modalBtn');
	modalBtn.forEach((modal, index) => {
		modal.addEventListener('click', () => {
			modalBox[index].classList.toggle('db');
		});
	});
	let deleteBtn = document.querySelectorAll('.delete');
	deleteBtn.forEach((btn, index) => {
		btn.addEventListener('click', () => {
			comments[index].style.display = 'none';
		});
	});
}

//add comment
const writeComment = document.querySelector('.write__comment-main');
const commentSection = document.querySelector('.main__comment-section');
writeComment.addEventListener('keypress', (e) => {
	if (e.key === 'Enter') {
		let newComment = document.createElement('div');
		newComment.setAttribute('class', 'comment__section');
		newComment.innerHTML = `<div class="comment__section-left">
		<img src="./assets/pic.jpg" alt="" />
	</div>
	<div class="comment__section-middle">
		<h2 class="name">Rupam Shil</h2>
		<p class="name__comment">  ${writeComment.value} </p>
		<div class="time">
			<p class="min">12m</p>
			<button class="like">Like</button>
			<button class="reply">Reply</button>
		</div>
		<!-- textreply  -->
						<textarea
							id=""
							class="write__comment reply__comment-box"
							placeholder="Share your thoughts"
						></textarea>
		<!-- textreply  -->
	</div>
	<div class="comment__section-right">
		<button class="modalBtn" onclick="toggleModal()"><i class="fas fa-ellipsis-h"></i></button>
		<div class="modal__box">
			<ul>
				<li class="delete">Delete</li>
				<li>Report</li>
				<li>Block</li>
			</ul>
		</div>
	</div>`;
		commentSection.appendChild(newComment);
		writeComment.value = '';
		if (e.preventDefault) e.preventDefault();
	}
});
//replytoggle
let replyBtn = document.querySelectorAll('.reply');
let replyComment = document.querySelectorAll('.reply__comment-box');
replyBtn.forEach((btn, index) => {
	btn.addEventListener('click', () => {
		replyComment[index].classList.toggle('db');
	});
});
//replyappend
let middleSec = document.querySelectorAll('.comment__section-middle');
replyComment.forEach((reply, index) => {
	reply.addEventListener('keypress', (e) => {
		if (e.key === 'Enter') {
			let newReply = document.createElement('div');
			newReply.setAttribute('class', 'reply__comment');
			newReply.innerHTML = `
			<div class="comment__section reply__section">
								<div class="comment__section-left">
									<img src="./assets/pic.jpg" alt="" />
								</div>
								<div class="comment__section-middle">
									<h2 class="name">Rupam Shil</h2>
									<p class="name__comment">${reply.value}</p>
									<div class="time">
										<p class="min">12m</p>
										<button class="like">Like</button>
									</div>
								</div>
								<div class="comment__section-right">
									<button class="modalBtn" onclick="toggleModal()">
										<i class="fas fa-ellipsis-h"></i>
									</button>
									<div class="modal__box">
										<ul>
											<li class="delete">Delete</li>
											<li>Report</li>
											<li>Block</li>
										</ul>
									</div>
								</div>
							</div>
			`;
			middleSec[index].appendChild(newReply);
			console.log(reply);
			reply.classList.remove('db');
		}
	});
});
